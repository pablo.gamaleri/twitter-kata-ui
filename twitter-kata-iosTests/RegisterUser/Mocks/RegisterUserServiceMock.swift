@testable import twitter_kata_ios

class RegisterUserServiceMock: RegisterUserService {
    
    var methodWasCalled = false
    
    func registerUser(username: (String), realName: (String), callback: @escaping (Bool) -> ()) {
        methodWasCalled = true
        callback(true)
    }
}
