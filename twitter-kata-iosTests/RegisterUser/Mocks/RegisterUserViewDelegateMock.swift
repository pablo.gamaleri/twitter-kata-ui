import Foundation
@testable import twitter_kata_ios

class RegisterUserViewDelegateMock: NSObject, RegisterUserViewDelegate {
    
    var methodWasCalled = false
    
    func displayRegistrationMessage(message: (String)) {
        methodWasCalled = true
    }
}
