@testable import twitter_kata_ios

class UsersAPIMock: UsersAPI {
    
    public var methodWasCalled = false
    
    func createUser(username:(String), realName:(String), callback:  @escaping (Bool) -> ()){
        methodWasCalled = true
        if(!username.isEmpty && !realName.isEmpty){
            callback(true)
        } else {
            callback(false)
        }
    }
}
