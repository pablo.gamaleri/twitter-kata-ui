import XCTest
@testable import twitter_kata_ios

class RegisterUserPresenterTests: XCTestCase {
    
    let mockRegisterUserViewDelegate = RegisterUserViewDelegateMock()
    let mockRegisterUserService = RegisterUserServiceMock()
    var registerUserPresenter: RegisterUserPresenter?
    
    override func setUp() {
        registerUserPresenter = RegisterUserPresenter(registerUserService: mockRegisterUserService)
        
        registerUserPresenter?.setViewDelegate(registerUserViewDelegate: mockRegisterUserViewDelegate)
    }
    
    func test_shouldCallUserServiceAndViewDelegate() {
        registerUserPresenter?.registerUser(username: "username", realName: "realName")
        XCTAssertEqual(mockRegisterUserService.methodWasCalled, true)
        XCTAssertEqual(mockRegisterUserViewDelegate.methodWasCalled, true)
    }
    
}
