import XCTest
@testable import twitter_kata_ios

class RegisterUserServiceTests: XCTestCase {

    var registerUserService: RegisterUserService?
    let usersApiMock = UsersAPIMock()
    
    override func setUp() {
        registerUserService = DefaultRegisterUserService(usersApi: usersApiMock)
    }
    
    func testRegister_shouldCallUsersAPI() throws {
        registerUserService!.registerUser(username: "username", realName: "realName", callback: { result in
                XCTAssertTrue(result)
            })
        XCTAssertTrue(usersApiMock.methodWasCalled)
    }
    
    func testRegister_withEmptyUsername_shouldCallbackFalse() throws {
        registerUserService!.registerUser(username: "", realName: "realName", callback: { result in
                XCTAssertFalse(result)
            })
    }

}

