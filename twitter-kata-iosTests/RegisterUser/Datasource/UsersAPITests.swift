import XCTest
@testable import twitter_kata_ios

class UsersAPITests: XCTestCase {

    let apiClientMock = APIClientMock()
    var usersApi: UsersAPI?
    
    override func setUp() {
        usersApi = DefaultUsersAPI(apiClient: apiClientMock)
    }
    
    func test_shouldCallApiClient(){
        usersApi?.createUser(username: "username", realName: "realName", callback: { created in })
        XCTAssertTrue(apiClientMock.postMethodWasCalled)
    }
}
