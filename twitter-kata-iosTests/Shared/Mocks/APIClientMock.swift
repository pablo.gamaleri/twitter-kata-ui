@testable import twitter_kata_ios
import Foundation

class APIClientMock : APIClient {
    
    var getMethodWasCalled = false
    var postMethodWasCalled = false
    
    func performGETRequest(url: URL, completion: @escaping (Result<NSDictionary>) -> Void) {
        getMethodWasCalled = true
    }
    
    func performPOSTRequest(url: URL, data: [String : Any], completion: @escaping (Result<NSDictionary>) -> Void) {
        postMethodWasCalled = true
    }
}
