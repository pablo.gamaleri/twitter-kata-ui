import XCTest
@testable import twitter_kata_ios

class TweetsPresenterTests: XCTestCase {

    let tweetsViewDelegateMock = TweetViewDelegateMock()
    let tweetsServiceMock = TweetServiceMock()
    var tweetsPresenter: TweetPresenter?
    
    override func setUp() {
        tweetsPresenter = TweetPresenter(tweetService: tweetsServiceMock)
        
        tweetsPresenter?.setViewDelegate(tweetViewDelegate: tweetsViewDelegateMock)
    }
    
    func testOnFollowUser_shouldCallTweetsServiceAndViewDelegate() {
        tweetsPresenter?.tweet(username: "username", tweet: "tweet")
        XCTAssertTrue(tweetsServiceMock.tweetMethodWasCalled)
        XCTAssertTrue(tweetsViewDelegateMock.displayTweetMessageWasCalled)
    }

    func testOnListingTweets_shouldCallTweetsServiceAndViewDelegate() {
        tweetsPresenter?.listUserTweets(username: "username")
        XCTAssertTrue(tweetsServiceMock.getTweetsMethodWasCalled)
        XCTAssertTrue(tweetsViewDelegateMock.displayTweetListWasCalled)
    }

}
