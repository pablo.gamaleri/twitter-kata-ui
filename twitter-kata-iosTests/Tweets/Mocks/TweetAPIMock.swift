@testable import twitter_kata_ios

class TweetAPIMock: TweetAPI {
    
    var tweetMethodWasCalled = false
    var getTweetsMethodWasCalled = false
    
    func tweet(username:(String), tweet:(String), callback: (Bool) -> ()){
        tweetMethodWasCalled = true
        callback(true)
    }
    
    func getTweets(username: (String), callback: @escaping ([String]?) -> ()) {
        getTweetsMethodWasCalled = true
        let tweets = ["tweet1", "tweet2"]
        callback(tweets)
    }
    
}
