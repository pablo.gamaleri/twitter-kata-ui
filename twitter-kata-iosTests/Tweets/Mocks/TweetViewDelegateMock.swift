import Foundation
@testable import twitter_kata_ios

class TweetViewDelegateMock: NSObject, TweetViewDelegate {
    
    var displayTweetMessageWasCalled = false
    var displayTweetListWasCalled = false
    
    func displayTweetMessage(message: (String)){
        displayTweetMessageWasCalled = true
    }
    
    func displayTweetList(list: ([String]?)) {
        displayTweetListWasCalled = true
    }
}
