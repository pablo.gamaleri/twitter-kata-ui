import Foundation
@testable import twitter_kata_ios

class TweetServiceMock: TweetService {

    var tweetMethodWasCalled = false
    var getTweetsMethodWasCalled = false
    
    func tweet(username: (String), tweet: (String), callback: @escaping (Bool) -> ()) {
        tweetMethodWasCalled = true
        callback(true)
    }
    
    func getTweets(username:(String), callback: @escaping ([String]?) -> ()) {
        getTweetsMethodWasCalled = true
        callback(nil)
    }

}
