import XCTest
@testable import twitter_kata_ios

class TweetServiceTests: XCTestCase {

    let tweetsApiMock = TweetAPIMock()
    var tweetsService: TweetService?
    
    override func setUp() {
        tweetsService = DefaultTweetService(tweetApi: tweetsApiMock)
    }
    
    func testOnTweetUser_shouldCallTweetsApiAndCallbackResult() {
        tweetsService?.tweet(username: "username", tweet: "tweet", callback: { result in
            XCTAssertTrue(result == true)
        })
        XCTAssertTrue(tweetsApiMock.tweetMethodWasCalled)
    }
    
    func testOnListingTweets_shouldCallTweetsApiAndCallbackList() {
        tweetsService?.getTweets(username: "username", callback: { tweets in
            XCTAssertNotNil(tweets)
        })
        XCTAssertTrue(tweetsApiMock.getTweetsMethodWasCalled)
    }

}

