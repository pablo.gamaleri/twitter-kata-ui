import XCTest
@testable import twitter_kata_ios

class TweetAPITests: XCTestCase {

    let apiClientMock = APIClientMock()
    var tweetApi: TweetAPI?
    
    override func setUp() {
        tweetApi = DefaultTweetAPI(apiClient: apiClientMock)
    }
    
    func testWhenTweeting_shouldCallApiClient(){
        tweetApi?.tweet(username: "username", tweet: "tweet", callback: { result in XCTAssertTrue(result) })
        XCTAssertTrue(apiClientMock.postMethodWasCalled)
    }
    
    func testWhenGettingUserTweets_shouldCallApiClientAndCallbackList(){
        tweetApi?.getTweets(username: "username", callback: { tweet in
            XCTAssertNotNil(tweet)
            XCTAssertTrue(tweet!.count > 0)
        })
        XCTAssertTrue(apiClientMock.getMethodWasCalled)
    }
}
