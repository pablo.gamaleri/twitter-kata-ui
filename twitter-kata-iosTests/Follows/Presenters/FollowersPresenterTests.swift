import XCTest
@testable import twitter_kata_ios

class FollowersPresenterTests: XCTestCase {

    let followersViewDelegateMock = FollowersViewDelegateMock()
    let followersServiceMock = FollowersServiceMock()
    var followersPresenter: FollowersPresenter?
    
    override func setUp() {
        followersPresenter = FollowersPresenter(followersService: followersServiceMock)
        
        followersPresenter?.setViewDelegate(followersViewDelegate: followersViewDelegateMock)
    }
    
    func testOnFollowUser_shouldCallFollowersServiceAndViewDelegate() {
        followersPresenter?.follow(username: "username", follower: "follower")
        XCTAssertTrue(followersServiceMock.followMethodWasCalled)
        XCTAssertTrue(followersViewDelegateMock.displayFollowingUserMessageWasCalled)
    }

    func testOnListingFollowers_shouldCallFollowersServiceAndViewDelegate() {
        followersPresenter?.listFollowers(username: "username")
        XCTAssertTrue(followersServiceMock.getFollowersMethodWasCalled)
        XCTAssertTrue(followersViewDelegateMock.displayFollowersListWasCalled)
    }
    
    func testOnListingFollowees_shouldCallFollowersServiceAndViewDelegate() {
        followersPresenter?.listFollowees(username: "username")
        XCTAssertTrue(followersServiceMock.getFolloweesMethodWasCalled)
        XCTAssertTrue(followersViewDelegateMock.displayFolloweesListWasCalled)
    }
}
