import XCTest
@testable import twitter_kata_ios

class FollowersServiceTests: XCTestCase {

    let followersApiMock = FollowersAPIMock()
    var followersService: FollowersService?
    
    override func setUp() {
        followersService = DefaultFollowersService(followersApi: followersApiMock)
    }
    
    func testOnFollowUser_shouldCallFollowersApiAndCallbackResult() {
        followersService?.follow(username: "username", follower: "follower", callback: { result in
            XCTAssertTrue(result == true)
        })
        XCTAssertTrue(followersApiMock.followMethodWasCalled)
    }
    
    func testOnListingFollowers_shouldCallFollowersApiAndCallbackList() {
        followersService?.getFollowers(username: "username", callback: { followers in
            XCTAssertNotNil(followers)
        })
        XCTAssertTrue(followersApiMock.getFollowersMethodWasCalled)
    }
    
    func testOnListingFollowees_shouldCallFollowersApiAndCallbackList() {
        followersService?.getFollowees(username: "username", callback: { followees in
            XCTAssertNotNil(followees)
        })
        XCTAssertTrue(followersApiMock.getFolloweesMethodWasCalled)
    }
}
