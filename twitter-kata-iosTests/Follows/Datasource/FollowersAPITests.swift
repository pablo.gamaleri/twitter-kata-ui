import XCTest
@testable import twitter_kata_ios

class FollowersAPITests: XCTestCase {

    let apiClientMock = APIClientMock()
    var followersApi: FollowersAPI?
    
    override func setUp() {
        followersApi = DefaultFollowersAPI(apiClient: apiClientMock)
    }
    
    func testWhenFollowingUser_shouldCallApiClient(){
        followersApi?.follow(username: "username", follower: "follower", callback: { result in XCTAssertTrue(result) })
        XCTAssertTrue(apiClientMock.postMethodWasCalled)
    }
    
    func testWhenGettingUserFollowers_shouldCallApiClientAndCallbackList(){
        followersApi?.getFollowers(username: "username", callback: { followers in
            XCTAssertNotNil(followers)
            XCTAssertTrue(followers!.count > 0)
        })
        XCTAssertTrue(apiClientMock.getMethodWasCalled)
    }
}
