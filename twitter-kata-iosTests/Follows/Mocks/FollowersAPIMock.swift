@testable import twitter_kata_ios

class FollowersAPIMock: FollowersAPI {
    
    var followMethodWasCalled = false
    var getFollowersMethodWasCalled = false
    var getFolloweesMethodWasCalled = false
    
    func follow(username:(String), follower:(String), callback: (Bool) -> ()){
        followMethodWasCalled = true
        callback(true)
    }
    
    func getFollowers(username:(String), callback: ([String]?) -> ()) {
        getFollowersMethodWasCalled = true
        let followers = ["username1", "username2"]
        callback(followers)
    }
    
    func getFollowees(username:(String), callback: ([String]?) -> ()) {
        getFolloweesMethodWasCalled = true
        let followees = ["username3", "username4"]
        callback(followees)
    }
    
}
