import Foundation
@testable import twitter_kata_ios

class FollowersServiceMock: FollowersService {

    var followMethodWasCalled = false
    var getFollowersMethodWasCalled = false
    var getFolloweesMethodWasCalled = false
    
    func follow(username:(String), follower:(String), callback:(Bool) -> ()){
        followMethodWasCalled = true
        callback(true)
    }
    
    func getFollowers(username: (String), callback: ([String]?) -> ()) {
        getFollowersMethodWasCalled = true
        callback(nil)
    }
    
    func getFollowees(username: (String), callback: ([String]?) -> ()) {
        getFolloweesMethodWasCalled = true
        callback(nil)
    }
}
