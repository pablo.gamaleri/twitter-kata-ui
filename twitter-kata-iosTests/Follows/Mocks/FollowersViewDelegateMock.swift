import Foundation
@testable import twitter_kata_ios

class FollowersViewDelegateMock: NSObject, FollowersViewDelegate {
    
    var displayFollowingUserMessageWasCalled = false
    var displayFollowersListWasCalled = false
    var displayFolloweesListWasCalled = false
    
    func displayFollowingUserMessage(message: (String)) {
        displayFollowingUserMessageWasCalled = true
    }
    
    func displayFollowersList(list: ([String]?)) {
        displayFollowersListWasCalled = true
    }
    
    func displayFolloweesList(list: ([String]?)) {
        displayFolloweesListWasCalled = true
    }
}
