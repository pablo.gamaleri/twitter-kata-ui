//
//  Networking.swift
//  Melody
//
//  Created by Matias Glessi on 26/11/2020.
//  Copyright © 2020 etermax. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}


protocol APIClient {
    func performGETRequest(url: URL, completion: @escaping (Result<NSDictionary>) -> Void)
    func performPOSTRequest(url: URL, data: [String:Any], completion: @escaping (Result<NSDictionary>) -> Void)
}


class ULRSessionAPIClient: APIClient {
    
    private let session: Session
    
    init(session: Session = URLSession.shared) {
        self.session = session
    }

    
    func performPOSTRequest(url: URL, data: [String:Any], completion: @escaping (Result<NSDictionary>) -> Void) {
        
        session.sendData(to: url, data: data) { (data, _, error) in
            guard let data = data else {
                completion(.failure(APIClientError.missingData))
                return
            }
          
            if let error = error {
                completion(.failure(error))
            }
            
            do {
                guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    completion(.failure(APIClientError.unknown))
                    return
                }
                completion(.success(dictionary))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    
    
    func performGETRequest(url: URL, completion: @escaping (Result<NSDictionary>) -> Void) {
        session.loadData(from: url) { (data, _, error) in
            
            guard let data = data else {
                completion(.failure(APIClientError.missingData))
                return
            }
            
            if let error = error {
                completion(.failure(error))
            }
            
            do {
                guard let dictionary = try JSONSerialization.jsonObject(with: data, options:[]) as? NSDictionary else {
                    completion(.failure(APIClientError.unknown))
                    return
                }
                completion(.success(dictionary))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
}





protocol Session {
    func loadData(from url: URL,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void)
    func sendData(to url: URL,
                  data: [String: Any],
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void)
}

extension URLSession: Session {
    func loadData(from url: URL,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = dataTask(with: url) { (data, response, error) in
            completionHandler(data, response, error)
        }

        task.resume()
    }
    
    func sendData(to url: URL,
                  data: [String: Any],
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
    
        guard let httpBody = try? JSONSerialization.data(withJSONObject: data, options: []) else {
            return
        }
        request.httpBody = httpBody

        let task = dataTask(with: request) { (data, response, error) in
            completionHandler(data, response, error)
        }
        
        task.resume()
    }
  
}

enum APIClientError: Error, Equatable {
    case missingData
    case unknown
}
