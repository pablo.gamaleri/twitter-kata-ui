import Foundation

class DefaultFollowersAPI: FollowersAPI {
    
    private var apiClient: APIClient
    
    init(apiClient: APIClient) {
        self.apiClient = apiClient
    }
    
    func follow(username: (String), follower: (String), callback: @escaping (Bool) -> ()) {
        let url = URL(string: "http://localhost:8080/followers/" + username)!
        let data = ["followerUsername":follower]
        apiClient.performPOSTRequest(url: url, data: data) { result in
            switch result {
                case .success( _) :
                    print(result)
                    callback(true)
                case .failure(let error):
                    print(error)
                    callback(false)
            }
        }
    }
    
    func getFollowers(username: (String), callback: @escaping ([String]?) -> ()) {
        let url = URL(string: "http://localhost:8080/followers/" + username)!
        apiClient.performGETRequest(url: url) { result in
            switch result {
                case .success(let dictionary) :
                    let followers: [String]? = dictionary["followers"] as? [String]
                    if(followers == nil){
                        callback(nil)
                    } else {
                        callback(followers)
                    }
                case .failure(let error):
                    print(error)
                    callback(nil)
            }
        }
    }
    
    func getFollowees(username: (String), callback: @escaping ([String]?) -> ()) {
        let url = URL(string: "http://localhost:8080/followees/" + username)!
        apiClient.performGETRequest(url: url) { result in
            switch result {
                case .success(let dictionary) :
                    let followees: [String]? = dictionary["followees"] as? [String]
                    if(followees == nil){
                        callback(nil)
                    } else {
                        callback(followees)
                    }
                case .failure(let error):
                    print(error)
                    callback(nil)
            }
        }
    }
    
}
