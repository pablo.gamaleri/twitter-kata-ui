import Foundation

class DefaultFollowersService: FollowersService {
    
    private var followersApi: FollowersAPI
    
    init(followersApi: FollowersAPI) {
        self.followersApi = followersApi
    }
    
    
    func follow(username: (String), follower: (String), callback: @escaping (Bool) -> ()) {
        followersApi.follow(username: username, follower: follower, callback: { result in callback(result) })
    }
    
    func getFollowers(username: (String), callback: @escaping ([String]?) -> ()) {
        followersApi.getFollowers(username: username, callback: { followers in callback(followers) })
    }
    
    func getFollowees(username: (String), callback: @escaping ([String]?) -> ()) {
        followersApi.getFollowees(username: username, callback: { followees in callback(followees) })
    }
}
