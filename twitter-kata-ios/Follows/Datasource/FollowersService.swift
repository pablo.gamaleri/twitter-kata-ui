protocol FollowersService {
    func follow(username:(String), follower:(String), callback: @escaping (Bool) -> ())
    func getFollowers(username:(String), callback: @escaping ([String]?) -> ())
    func getFollowees(username:(String), callback: @escaping ([String]?) -> ())
}
