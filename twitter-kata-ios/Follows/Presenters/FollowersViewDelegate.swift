import Foundation

protocol FollowersViewDelegate: NSObjectProtocol {
    
    func displayFollowingUserMessage(message:(String))
    func displayFollowersList(list:([String]?))
    func displayFolloweesList(list:([String]?))
}
