import Foundation

class FollowersPresenter {
    
    private let followersService: FollowersService
    weak private var followersViewDelegate : FollowersViewDelegate?
    
    init(followersService:FollowersService){
        self.followersService = followersService
    }
    
    func setViewDelegate(followersViewDelegate:FollowersViewDelegate?){
        self.followersViewDelegate = followersViewDelegate
    }
    
    func follow(username:(String), follower:(String)) {
        followersService.follow(username: username, follower: follower, callback: { result in
            if(result == true){
                let message = follower + " is now following " + username
                self.followersViewDelegate?.displayFollowingUserMessage(message: message)
            } else{
                self.followersViewDelegate?.displayFollowingUserMessage(message: "error")
            }
        })
    }
    
    func listFollowers(username:(String)) {
        followersService.getFollowers(username: username, callback:{ followers in
            self.followersViewDelegate?.displayFollowersList(list: followers)
        })
    }
    
    func listFollowees(username:(String)) {
        followersService.getFollowees(username: username, callback:{ followees in
            self.followersViewDelegate?.displayFolloweesList(list: followees)
        })
    }
    
}
