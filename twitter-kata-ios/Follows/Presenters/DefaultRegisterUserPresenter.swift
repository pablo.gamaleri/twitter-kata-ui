import Foundation

class RegisterUserPresenter {
    
    private let registerUserService: RegisterUserService
    weak private var registerUserViewDelegate : RegisterUserViewDelegate?
    
    init(registerUserService:RegisterUserService){
        self.registerUserService = registerUserService
    }
    
    func setViewDelegate(registerUserViewDelegate:RegisterUserViewDelegate?){
        self.registerUserViewDelegate = registerUserViewDelegate
    }
    
    func registerUser(username:(String), realName:(String)){
        registerUserService.registerUser(username: username, realName: realName, callback: { created in
                if created == true {
                    self.registerUserViewDelegate?.displayRegistrationMessage(message: "Done!")
                } else {
                    self.registerUserViewDelegate?.displayRegistrationMessage(message: "An error occurred")
                }
        })
    }
    
}
