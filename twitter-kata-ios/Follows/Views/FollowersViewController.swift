import UIKit

class FollowersViewController: UIViewController, UITableViewDataSource, FollowersViewDelegate {
    
    @IBOutlet var yourUsernameTxt: UITextField!
    @IBOutlet var usernameToBeFollowedTxt: UITextField!
    @IBOutlet var followBtn: UIButton!
    
    @IBOutlet var followersForUserTxt: UITextField!
    @IBOutlet var listFollowersBtn: UIButton!
    
    @IBOutlet var followeesForUserTxt: UITextField!
    @IBOutlet var listFolloweesBtn: UIButton!
    
    @IBOutlet var followsTableView: UITableView!
    var follows: [String] = []
    let cellReuseIdentifier = "cell"
    
    var userText: String?
    
    let followersPresenter = FollowersPresenter(followersService: DefaultFollowersService(followersApi: DefaultFollowersAPI(apiClient: ULRSessionAPIClient())))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        yourUsernameTxt.placeholder = "username"
        usernameToBeFollowedTxt.placeholder = "user to be followed"
        
        followersForUserTxt.placeholder = "username"
        followeesForUserTxt.placeholder = "username"
        
        followersPresenter.setViewDelegate(followersViewDelegate: self)
        yourUsernameTxt.text = userText
        self.followsTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        followsTableView.dataSource = self
    }
    
    @IBAction func followUserAction(_ sender: Any) {
        followersPresenter.follow(username: usernameToBeFollowedTxt.text!, follower: yourUsernameTxt.text!)
    }
    
    @IBAction func listFollowersAction(_ sender: Any) {
        followersPresenter.listFollowers(username: followersForUserTxt.text!)
    }
    
    @IBAction func listFolloweesAction(_ sender: Any) {
        followersPresenter.listFollowees(username: followeesForUserTxt.text!)
    }
    
    func displayFollowingUserMessage(message: (String)) {
        displayMessage(message: message)
    }
    
    func displayFollowersList(list: ([String]?)) {
        if(list != nil){
            displayInTableView(follows: list!)
        }
    }
    func displayInTableView(follows:([String])){
        DispatchQueue.main.async {
            self.follows = follows
            self.followsTableView.reloadData()
        }
    }
    
    func displayFolloweesList(list: ([String]?)) {
        if(list != nil){
            displayInTableView(follows: list!)
        }
    }
    
    func displayMessage(message: (String)) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Followers", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.follows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = (self.followsTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell?)!
        cell.textLabel?.text = self.follows[indexPath.row]
        return cell
    }
    
}
