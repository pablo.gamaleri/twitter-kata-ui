import UIKit

class TweetsViewController: UIViewController, UITableViewDataSource, TweetViewDelegate {
    
    @IBOutlet var yourUsernameTxt: UITextField!
    @IBOutlet var tweetTxt: UITextField!
    @IBOutlet var tweetBtn: UIButton!
    
    @IBOutlet var tweetsForUserTxt: UITextField!
    @IBOutlet var listTweetsBtn: UIButton!
    
    @IBOutlet var tweetsTableView: UITableView!
    
    var tweets: [String] = []
    let cellReuseIdentifier = "cell"
    
    let tweetPresenter = TweetPresenter(tweetService: DefaultTweetService(tweetApi: DefaultTweetAPI(apiClient: ULRSessionAPIClient())))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        yourUsernameTxt.placeholder = "your username"
        tweetTxt.placeholder = "your tweet"
        tweetsForUserTxt.placeholder = "username"
        tweetPresenter.setViewDelegate(tweetViewDelegate: self)
        self.tweetsTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        tweetsTableView.dataSource = self
    }
    
    @IBAction func tweetAction(_ sender: Any) {
        tweetPresenter.tweet(username: yourUsernameTxt.text!, tweet: tweetTxt.text!)
    }
    
    @IBAction func listUserTweetsAction(_ sender: Any) {
        tweetPresenter.listUserTweets(username: tweetsForUserTxt.text!)
    }
    
    func displayTweetMessage(message: (String)) {
        displayMessage(message: message)
        DispatchQueue.main.async { self.tweetTxt.text = "" }
    }
    
    func displayTweetList(list: ([String]?)) {
        if(list != nil){
            displayInTableView(tweets: list!)
        }
    }
    
    func displayInTableView(tweets:([String])){
        DispatchQueue.main.async {
            self.tweets = tweets
            self.tweetsTableView.reloadData()
        }
    }
    
    func displayMessage(message: (String)) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Tweets", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tweets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = (self.tweetsTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell?)!
        cell.textLabel?.text = self.tweets[indexPath.row]
        return cell
    }
    
}
