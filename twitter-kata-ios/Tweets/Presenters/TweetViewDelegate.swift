import Foundation

protocol TweetViewDelegate: NSObjectProtocol {
    func displayTweetMessage(message: (String))
    func displayTweetList(list: ([String]?))
}
