import Foundation

class TweetPresenter {
    
    private let tweetService: TweetService
    weak private var tweetViewDelegate : TweetViewDelegate?
    
    init(tweetService:TweetService){
        self.tweetService = tweetService
    }
    
    func setViewDelegate(tweetViewDelegate:TweetViewDelegate?){
        self.tweetViewDelegate = tweetViewDelegate
    }
    
    func tweet(username:(String), tweet:(String)) {
        tweetService.tweet(username: username, tweet: tweet, callback: { result in
            if(result == true){
                let message = "Tweet was posted."
                self.tweetViewDelegate?.displayTweetMessage(message: message)
            } else{
                self.tweetViewDelegate?.displayTweetMessage(message: "Error")
            }
        })
    }
    
    func listUserTweets(username:(String)) {
        tweetService.getTweets(username: username, callback:{ tweets in
            self.tweetViewDelegate?.displayTweetList(list: tweets)
        })
    }
}
