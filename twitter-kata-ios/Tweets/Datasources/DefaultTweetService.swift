import Foundation

class DefaultTweetService: TweetService {
    
    private var tweetApi: TweetAPI
    
    init(tweetApi: TweetAPI) {
        self.tweetApi = tweetApi
    }
    
    func tweet(username: (String), tweet: (String), callback: @escaping (Bool) -> ()) {
        tweetApi.tweet(username: username, tweet: tweet, callback: { result in callback(result) })
    }
    
    func getTweets(username: (String), callback: @escaping ([String]?) -> ()) {
        tweetApi.getTweets(username: username, callback: { tweets in callback(tweets) })
    }
}
