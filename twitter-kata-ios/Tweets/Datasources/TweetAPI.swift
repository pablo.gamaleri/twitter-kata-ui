protocol TweetAPI {
    func tweet(username:(String), tweet:(String), callback: @escaping (Bool) -> ())
    func getTweets(username:(String), callback: @escaping ([String]?) -> ())
}
