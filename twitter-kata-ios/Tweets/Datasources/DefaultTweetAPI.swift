import Foundation

class DefaultTweetAPI: TweetAPI {
    
    private var apiClient: APIClient
    
    init(apiClient: APIClient) {
        self.apiClient = apiClient
    }
    
    func tweet(username: (String), tweet: (String), callback: @escaping (Bool) -> ()) {
        let url = URL(string: "http://localhost:8080/tweets/" + username)!
        let data = ["message":tweet]
        apiClient.performPOSTRequest(url: url, data: data) { result in
            switch result {
                case .success( _) :
                    print(result)
                    callback(true)
                case .failure(let error):
                    print(error)
                    callback(false)
            }
        }
    }
    
    func getTweets(username: (String), callback: @escaping ([String]?) -> ()) {
        let url = URL(string: "http://localhost:8080/tweets/" + username)!
        apiClient.performGETRequest(url: url) { result in
            switch result {
                case .success(let dictionary) :
                    let tweets: [String]? = dictionary["tweets"] as? [String]
                    if(tweets == nil){
                        callback(nil)
                    } else {
                        callback(tweets)
                    }
                case .failure(let error):
                    print(error)
                    callback(nil)
            }
        }
    }
    
}
