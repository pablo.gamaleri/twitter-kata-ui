import Foundation

protocol UsersAPI {
    func createUser(username:(String), realName:(String), callback:  @escaping (Bool) -> ())
}
