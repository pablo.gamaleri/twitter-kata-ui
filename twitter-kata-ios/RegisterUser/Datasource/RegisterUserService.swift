import Foundation

protocol RegisterUserService {
    func registerUser(username:(String), realName:(String), callback: @escaping  (Bool) -> ())
}
