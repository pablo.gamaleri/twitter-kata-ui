import Foundation

class DefaultRegisterUserService: RegisterUserService {

    private var usersApi: UsersAPI
    
    init(usersApi: UsersAPI) {
        self.usersApi = usersApi
    }
    
    func registerUser(username:(String), realName:(String), callback: @escaping (Bool) -> ()) {
        if(!username.isEmpty && !realName.isEmpty){
            usersApi.createUser(username: username, realName: realName, callback: { created in
                    callback(created)
                })
        } else {
            callback(false)
        }
    }
}
