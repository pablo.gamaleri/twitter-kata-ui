import Foundation

class DefaultUsersAPI: UsersAPI {
    private var apiClient: APIClient
    
    init(apiClient: APIClient) {
        self.apiClient = apiClient
    }
    
    func createUser(username:(String), realName:(String), callback:  @escaping (Bool) -> ()) {
        let url = URL(string: "http://localhost:8080/users")!
        let data = ["username":username, "name":realName]
        apiClient.performPOSTRequest(url: url, data: data) { result in
            switch result {
                case .success(let dictionary):
                    let message: String? = dictionary["message"] as? String
                    if(message != nil && message == "OK"){
                        callback(true)
                    } else{
                        callback(false)
                    }
                case .failure(let error):
                    print(error)
                    callback(false)
            }
        }
    }
    
}
