import Foundation

protocol RegisterUserViewDelegate: NSObjectProtocol {
    func displayRegistrationMessage(message:(String))
}
