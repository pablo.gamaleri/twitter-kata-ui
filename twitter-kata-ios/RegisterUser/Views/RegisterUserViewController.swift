import UIKit

class RegisterUserViewController: UIViewController, RegisterUserViewDelegate {

    @IBOutlet var usernameTxt: UITextField!
    @IBOutlet var realNameTxt: UITextField!
    @IBOutlet var registerBtn: UIButton!
    
    private var registerUserPresenter = RegisterUserPresenter(registerUserService: DefaultRegisterUserService(usersApi: DefaultUsersAPI(apiClient: ULRSessionAPIClient())))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerUserPresenter.setViewDelegate(registerUserViewDelegate: self)
        usernameTxt.placeholder = "username"
        realNameTxt.placeholder = "real name"
    }
    
    @IBAction func registerUserAction(_ sender: Any) {
        registerUserPresenter.registerUser(
            username: usernameTxt.text!, realName: realNameTxt.text!)
    }
    
    func displayRegistrationMessage(message: (String)) {
        DispatchQueue.main.async { [self] in
            let alert = UIAlertController(title: "User registration", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: nil))
            self.present(alert, animated: true)
            if(message == "Done!"){
                self.usernameTxt.text = ""
                self.realNameTxt.text = ""
            }
        }
    }
    
}

